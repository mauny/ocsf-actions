# Actions


## Community management

* Community manager (partially funded)
* Sponsor
    - Meetups (faire une annonce dans la caml-list)
    - Workshops (OCaml, autres?)
    - Major conferences?
    - OCamlCon, a major conference about OCaml, in the style of PyCon.


## Support testing and release work/infrastructure

* Develop tools to debug OPAM package dependencies

## Development (libraries, tools, future features, ...)

* Improve error messages of the compiler.

* Improve opam.ocaml.org to help programmers match packages with their needs.

  - Create a uniform package documentation format.
  - Centralize community feedback about packages.

* Opam for MS Windows, improve MS Windows support in general.

* Make sure that "simple things are simple to do". Follow Python
  philosophy to make OCaml a good language for Rapid Application
  Development.

## Research

* Rewriting the type system

   - The idea is to use a constraint-based approach and have a more modular
     implementation, so that the type system is easier to modify in the
     future.

   - This will probably include an explicitly typed tree after the
     elaboration that will have its own separate typechecker.

   - Ideally, the implementation should be functional, even if parts of it
     could use side effects locally.

* Modular implicits

   - Extend the module language in preparation of implicit modules, this
     should allow to write all uses of "implicit modules" explicitly.

   - Typecheck implicits and elaborate them into the explicit sublanguage.

   - We probably wish to formalize the meta-theory in Coq...

* (insert your item here)

## Documentation

* Financially support the writing of tutorials on:

   - Multicore OCaml (which could become a book)
   - Lwt
   - Eliom/Ocsigen
   - OCaml for the Python programmer / Java programmer / C programmer.
   - Opam
   - Dune
   - TensorFlow
   - LLVM
   - (insert here your favorite library/tool/other)

* Financially support the presence of OCaml on StackOverflow

   - Create and answer questions about tasks commonly encountered
     while developing applications.

   - When there is no satisfying answer, provide some feedback to
     library developers to improve API.

   - Import the important questions of the caml-list archives in
     StackOverflow.

## Learn-OCaml (funded)

* The OCaml MOOC
* Educational content
* The Learn-OCaml software architecture

## Other?
